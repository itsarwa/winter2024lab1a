import java.util.Random;

public class Calculator{
    private static Random	random = new Random();
	
	public static int add(int x, int y){
		return x+y;
	}
	public static double sqrt( int z){
		return Math.sqrt(z);
	}
	public static int random(){
		return random.nextInt();
	}
	public static int divide (int a, int b){
		return a/b;
	}
}
